{ config, lib, pkgs, ... }:

with lib;

let
  snmRelease = "nixos-24.05";
  snmHash = "sha256:05b77fhmxggqw3sd0p766ai34qqqdxs6jzl7g7881bc8ylrv90f2";

  concatMapLines = f: l: lib.concatStringsSep "\n" (map f l);

  cfg = config.services.mailing-lists;
  stateDir = "/var/lib/mlmmj";
  spoolDir = "/var/spool/mlmmj";
  listDir = domain: list: "${spoolDir}/${domain}/${list}";
  listCtl = domain: list: "${listDir domain list}/control";
  transport = domain: list: "${domain}--${list}@local.list.mlmmj mlmmj:${domain}/${list}";
  virtual = domain: list: "${list}@${domain} ${domain}--${list}@local.list.mlmmj";
  alias = domain: list: "${list}: \"|${pkgs.mlmmj}/bin/mlmmj-receive -L ${listDir domain list}/\"";
  subjectPrefix = list: "[${list}]";
  listAddress = domain: list: "${list}@${domain}";
  customHeaders = domain: list: [
    "List-Id: ${list}"
    "Reply-To: ${list}@${domain}"
    "List-Post: <mailto:${list}@${domain}>"
    "List-Help: <mailto:${list}+help@${domain}>"
    "List-Subscribe: <mailto:${list}+subscribe@${domain}>"
    "List-Unsubscribe: <mailto:${list}+unsubscribe@${domain}>"
  ];
  footer = domain: list: "To unsubscribe send a mail to ${list}+unsubscribe@${domain}";
  createList = d: l:
    let ctlDir = listCtl d l; in
    ''
      for DIR in incoming queue queue/discarded archive text subconf unsubconf \
                 bounce control moderation subscribers.d digesters.d requeue \
                 nomailsubs.d
      do
             mkdir -p '${listDir d l}'/"$DIR"
      done
      ${pkgs.coreutils}/bin/mkdir -p ${ctlDir}
      echo ${listAddress d l} > '${ctlDir}/listaddress'
      [ ! -e ${ctlDir}/customheaders ] && \
          echo "${lib.concatStringsSep "\n" (customHeaders d l)}" > '${ctlDir}/customheaders'
      [ ! -e ${ctlDir}/footer ] && \
          echo ${footer d l} > '${ctlDir}/footer'
      [ ! -e ${ctlDir}/prefix ] && \
          echo ${subjectPrefix l} > '${ctlDir}/prefix'
    '';
in

{

  imports = [
    (fetchTarball {
      url = "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/${snmRelease}/nixos-mailserver-${snmRelease}.tar.gz";
      sha256 = snmHash;
    })
  ];

  ###### interface

  options = {

    services.mailing-lists = {

      enable = mkOption {
        type = types.bool;
        default = false;
        description = lib.mdDoc "Enable mlmmj";
      };

      user = mkOption {
        type = types.str;
        default = "mlmmj";
        description = lib.mdDoc "mailinglist local user";
      };

      group = mkOption {
        type = types.str;
        default = "mlmmj";
        description = lib.mdDoc "mailinglist local group";
      };

      listDomain = mkOption {
        type = types.str;
        default = "localhost";
        description = lib.mdDoc "Set the mailing list domain";
      };

      mailHost = mkOption {
        type = types.str;
        default = "localhost";
        description = lib.mdDoc "Set the mail host FQDN.";
      };

      mailLists = mkOption {
        type = types.listOf types.str;
        default = [];
        description = lib.mdDoc "The collection of hosted maillists";
      };

      maintInterval = mkOption {
        type = types.str;
        default = "20min";
        description = lib.mdDoc ''
          Time interval between mlmmj-maintd runs, see
          {manpage}`systemd.time(7)` for format information.
        '';
      };

    };

  };

  ###### implementation

  config = mkIf cfg.enable {

    users.users.${cfg.user} = {
      description = "mlmmj user";
      home = stateDir;
      createHome = true;
      uid = config.ids.uids.mlmmj;
      group = cfg.group;
      useDefaultShell = true;
    };

    users.groups.${cfg.group} = {
      gid = config.ids.gids.mlmmj;
    };

    mailserver = {
      enable = true;
      fqdn = cfg.mailHost;
      domains = [ cfg.listDomain ];
      certificateScheme = "acme-nginx";
      enableImap = false;
      enableImapSsl = false;
      enableSubmission = false;
      enableSubmissionSsl = true;
    };

    services.postfix = {
      masterConfig.mlmmj = {
        type = "unix";
        private = true;
        privileged = true;
        chroot = false;
        wakeup = 0;
        command = "pipe";
        args = [
          "flags=ORhu"
          "user=mlmmj"
          "argv=${pkgs.mlmmj}/bin/mlmmj-receive"
          "-F"
          "-L"
          "${spoolDir}/$nexthop"
        ];
      };
      extraAliases = concatMapLines (alias cfg.listDomain) cfg.mailLists;
      extraConfig = "propagate_unmatched_extensions = virtual";
      virtual = concatMapLines (virtual cfg.listDomain) cfg.mailLists;
      transport = concatMapLines (transport cfg.listDomain) cfg.mailLists;
    };

    environment.systemPackages = [ pkgs.mlmmj ];

    system.activationScripts.mlmmj = ''
          ${pkgs.coreutils}/bin/mkdir -p ${stateDir} ${spoolDir}/${cfg.listDomain}
          ${pkgs.coreutils}/bin/chown -R ${cfg.user}:${cfg.group} ${spoolDir}
          ${concatMapLines (createList cfg.listDomain) cfg.mailLists}
          ${pkgs.postfix}/bin/postmap /etc/postfix/virtual
          ${pkgs.postfix}/bin/postmap /etc/postfix/transport
      '';

    systemd.services.mlmmj-maintd = {
      description = "mlmmj maintenance daemon";
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        ExecStart = "${pkgs.mlmmj}/bin/mlmmj-maintd -F -d ${spoolDir}/${cfg.listDomain}";
      };
    };

    systemd.timers.mlmmj-maintd = {
      description = "mlmmj maintenance timer";
      timerConfig.OnUnitActiveSec = cfg.maintInterval;
      wantedBy = [ "timers.target" ];
    };
  };

}
