{ config, lib, pkgs, ... }:

let

  cfg = config.services.glances-web;
  glances-port = 61208;

in 

{
  options = with lib; {
    services.glances-web = with types; {
      enable = mkEnableOption "Enable glances";
      virtualHost = mkOption {
        type = nullOr str;
        default = null;
        description = mdDoc ''
          Name of the nginx virtualhost to setup and use. Disabled if null.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.glances-web = {
      description = "Web-Based System Monitoring";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      path = [ pkgs.glances ];
      script = "glances --light -w";
    };
    services.nginx = lib.mkIf (cfg.virtualHost != null) {
      enable = true;
      virtualHosts.${cfg.virtualHost} = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${builtins.toString glances-port}";
        };
      };
    };
  };
}
