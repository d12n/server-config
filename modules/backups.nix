{ config, ... }:

{
  # Make sure btrbk can get necessary privileges.
  security.doas = {
    extraRules =[
      {
        users = [ "btrbk" ];
        noPass = true;
        cmd = "/run/current-system/sw/bin/btrfs";
      }
      {
        users = [ "btrbk" ];
        noPass = true;
        cmd = "/run/current-system/sw/bin/mkdir";
      }
      {
        users = [ "btrbk" ];
        noPass = true;
        cmd = "/run/current-system/sw/bin/readlink";
      }
    ];
  };

  # Enable snapshots of /var directory.
  services.btrbk = {
    instances.var = {
      onCalendar = "hourly";
      settings = {
        backend = "btrfs-progs-doas";
        timestamp_format = "long";
        snapshot_preserve = "14d";
        snapshot_preserve_min = "2d";
        volume."/" = {
          subvolume.var = {
            snapshot_create = "always";
            snapshot_dir = ".snapshots";
          };
        };
      };
    };
  };

  # Enable daily remote backups of /var/lib directory.
  services.restic = {
    backups.varlib = {
      initialize = true;
      repository = "rclone:stackstorage:${config.networking.hostName}";
      paths = [ "/var/lib" ];
      passwordFile = "/etc/nixos/secrets/restic_password.txt";
      rcloneConfigFile = "/etc/nixos/secrets/rclone_stackstorage.conf";
      pruneOpts = [
        "--keep-weekly 4"
        "--keep-monthly 3"
      ];
    };
    backups.varspool = {
      initialize = true;
      repository = "rclone:stackstorage:${config.networking.hostName}";
      paths = [ "/var/spool" ];
      passwordFile = "/etc/nixos/secrets/restic_password.txt";
      rcloneConfigFile = "/etc/nixos/secrets/rclone_stackstorage.conf";
      pruneOpts = [
        "--keep-weekly 2"
        "--keep-monthly 1"
      ];
    };
  };

}
