{ config, lib, ... }:

let

  cfg = config.services.linklog;
  package = import (fetchTarball {
    url = "https://code.jboy.space/linklog/tarball/trunk/linklog.tar.gz";
  });

in 

{
  options = with lib; {
    services.linklog = with types; {
      enable = mkEnableOption "Enable linklog";
      dataDir = mkOption {
        type = str;
        default = "/var/lib/linklog";
        description = mdDoc "Data directory where the service's database is stored.";
      };
      user = mkOption {
        type = oneOf [ str int ];
        default = "linklog";
        description = ''
          The user the service will use.
        '';
      };
      group = mkOption {
        type = oneOf [ str int ];
        default = "linklog";
        description = ''
          The group the service will use.
        '';
      };
      port = mkOption {
        type = oneOf [ str int ];
        default = 7122;
        description = ''
          The port the service will use internally.
        '';
      };

      virtualHost = mkOption {
        type = nullOr str;
        default = null;
        description = mdDoc ''
          Name of the nginx virtualhost to setup and use. Disabled if null.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    users.groups.${cfg.user} = { };
    users.users.${cfg.user} = {
      createHome = true;
      description = "Linklog service user";
      group = "${cfg.group}";
      home = "${cfg.dataDir}";
      isSystemUser = true;
    };
    services.uwsgi = {
      enable = true;
      user = "${cfg.user}";
      plugins = [ "python3" ];
      instance = {
        chdir = package;
        env = [ "FLASK_INSTANCE_PATH=${cfg.dataDir}" ];
        http = ":${builtins.toString cfg.port}";
        module = "app:app";
        pythonPackages = p: with p; [ flask flask_login flask_wtf bcrypt peewee mistune smartypants ];
        type = "normal";
      };
    };
    services.nginx = lib.mkIf (cfg.virtualHost != null) {
      enable = true;
      virtualHosts.${cfg.virtualHost} = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${builtins.toString cfg.port}";
        };
        locations."/static" = {
          root = "${package}";
        };
      };
    };
  };
}
