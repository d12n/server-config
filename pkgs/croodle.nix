{ lib
, pkgs
, dataDir ? "/var/lib/croodle"
, debug ? false
, ... }:

let
  version = "0.7.0";
  configFile = pkgs.writeText "croodle-config.php" ''
    <?php

    return array(
      /*
       * dataDir (String)
       * relative or absolute path to folder where polls are stored
       */
      'dataDir' => getenv('CROODLE__DATA_DIR') ?: '${dataDir}/',

      /*
       * debug (Boolean)
       * controls Slim debug mode
       */
      'debug' => getenv('CROODLE__DEBUG') ?: ${lib.boolToString debug}
    );
  '';

in

pkgs.stdenv.mkDerivation rec {
  pname = "croodle";
  inherit version;

  src = pkgs.fetchurl {
    url = "https://github.com/jelhan/${pname}/releases/download/v${version}/${pname}-v${version}.tar.gz";
    hash = "sha256-WDiM7W7jR+6EFSnYJ024FFqvs7UfWyPfpeFZ0QY3TIk=";
  };

  sourceRoot = ".";

  installPhase = ''
    rm env-vars
    mkdir -p $out
    cp -r * $out
    rm -rf $out/data
    cp ${configFile} $out/api/config.php
  '';

  meta = with lib; {
    description = "A web application to schedule a date or to do a poll on a general topics.";
    homepage = "https://github.com/jelhan/croodle";
    license = licenses.mit;
    platforms = platforms.all;
  };
}
