Boot from [live CD](https://nixos.org/download.html#nixos-iso)

```sh
sudo su
# partition disk
parted /dev/vda -- mklabel msdos
parted /dev/vda -- mkpart primary 1MiB 100%
pvcreate /dev/vda1
vgcreate vg /dev/vda1
lvcreate -L 8G -n swap vg
lvcreate -l '100%FREE' -n root vg
mkfs.btrfs -L root /dev/vg/root
mount -t btrfs -L root /mnt
btrfs subvolume create /mnt/nixos
umount /mnt
mount -t btrfs -o subvol=nixos -L root /mnt
btrfs subvolume create /mnt/var
btrfs subvolume create /mnt/home
btrfs subvolume create /mnt/tmp
mkswap -L swap /dev/vg/swap
swapon -L swap
# install system
cd /mnt
git clone https://gitlab.com/d12n/server-config.git etc/nixos
cd etc/nixos
nix-shell -p transcrypt --run 'transcrypt -c aes-256-cbc -p CONFIG_PASSWORD'
nix-channel --update
nixos-generate-config --root /mnt
nixos-install --root /mnt
```
