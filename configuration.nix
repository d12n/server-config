# vim: sw=2 ts=2 et ai smartindent
# Help is available in the configuration.nix(5) man page and
# in the NixOS manual (accessible by running ‘nixos-help’).

{ config
, pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05-small.tar.gz") {}
, lib
, ... }:

let
  secrets = import ./secrets;
  d12nSite = import (fetchTarball {
    url = "https://gitlab.com/d12n/site/-/archive/trunk/site-trunk.tar.gz";
  });
  emailAddress = lib.concatStringsSep "@" [ "infra" "leiden.digital" ];
  ports = {
    etherpad = 8060;
    hedgedoc = 8040;
    ssh = 2202;
    taguette = 8090;
    gotosocial = 8050;
  };
  jitsiDomain = "meet.leiden.digital";
  gotosocialDomain = "fed.leiden.digital";
  gotosocialDir = "/var/lib/gotosocial";
  hedgedocDomain = "edit.leiden.digital";
  hedgedocDB = "/var/lib/hedgedoc/hedgedoc.db";

in
{
  imports = [
    ./hardware-configuration.nix
    ./modules/croodle.nix
    ./modules/glances-web.nix
    ./modules/linklog.nix
    ./modules/mailing-lists.nix
    ./modules/backups.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/vda";

  networking = {
    hostName = "ensbo";
    useDHCP = false;
    interfaces.ens3 = {
      useDHCP = false;
      ipv4.addresses = [ { address = "5.2.72.25"; prefixLength = 25; } ];
      ipv6.addresses = [ { address = "2a04:52c0:101:5f5::"; prefixLength = 64; } ];
    };
    defaultGateway = { address = "5.2.65.129"; interface = "ens3"; };
    defaultGateway6 = { address = "2a04:52c0:101::1"; interface = "ens3"; };
    nameservers = [
      "185.31.172.240"
      "89.188.29.4"
      "2a01:1b0:7999:446::1:4"
      "2a01:6340:1:20:4::10"
    ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    motd = "This server kills fascists.";
    mutableUsers = false;
    users.jboy = {
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK/FW+aFqFojO/MvpQsmDePBiTqgZh2uesvzxcPxqp+z cardno:23_858_739"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCVznnaBsKGar3x802yZ667jsWJ3YRkuisFag/6eh9UgFgIPhPsSNAnxC58TNmjst8zTWdM0pqU4fWHlNH7seSBbWn7GpJ071mMOLM52KyRmFd/T3tYexBU2/K20UhhRry2a17u/nu3STQwiP+dsutLKQ2YHy4Z37moLcgDswe+ms+pjhHHHxcdbYENv1Xi1QGA0XOkehSu2Xoom0snIo9iSbzaazPq+TAZQPDrOdXcni4NV+8B16I+DSEVIbVx/hPkwHmei8cXGMa8GcaUfhb4ncSr3BbW01drpo7g4YZxQMwrQzV9WjHmjHKjuhfKH+MIeWR4G573WrrYEGKMfNiX cardno:000606911493"
      ];
      extraGroups = [ "wheel" ];
    };
  };

  security = {
    sudo.enable = false;
    doas = {
      enable = true;
      extraRules = [
        {
          users = [ "jboy" ];
          keepEnv = true;
          noPass = true;
        }
      ];
    };
  };

  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    channel = https://nixos.org/channels/nixos-24.05-small;
  };

  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 60d";
    };
    optimise ={
      automatic = true;
      dates = [ "weekly" ];
    };
  };

  documentation.man.generateCaches = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    binutils
    curl
    file
    gitFull
    nix-bash-completions
    tmux
    transcrypt
    wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs.vim.defaultEditor = true;

  services.mailing-lists = {
    enable = true;
    listDomain = "lists.leiden.digital";
    mailHost = "mx.leiden.digital";
    mailLists = [
      "testing"
      "d12n-discuss"
    ];
    maintInterval = "5min";
  };

  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
    oci-containers = {
      backend = "podman";
      containers = {
        etherpad-lite = {
          image = "fuerst/etherpad-docker";
          ports = [ "${builtins.toString ports.etherpad}:9001" ];
          volumes = [ "/var/lib/etherpad-lite:/opt/etherpad-lite/var" ];
          environment = {
            DB_TYPE = "sqlite";
            TRUST_PROXY = "true";
            TITLE = "Leiden Etherpad";
            DEFAULT_PAD_TEXT = "Welcome to the leiden.digital Etherpad instance!";
          };
        };
        taguette = {
          image = "quay.io/remram44/taguette";
          ports = [ "${builtins.toString ports.taguette}:7465" ];
          volumes = [
            "/var/lib/taguette:/data"
          ];
          cmd = [ "server" "/data/taguette_config.py" ];
        };
      };
    };
  };

  systemd.services = {
    podman-taguette = {
      preStart = "mkdir -p /var/lib/taguette";
    };
    podman-etherpad-lite = {
      preStart = "mkdir -p /var/lib/etherpad-lite";
    };
    btrbk-var = {
      preStart = "doas -n mkdir -p /.snapshots";
    };
    hedgedoc-db = {
      preStart = "doas -n -u hedgedoc touch ${hedgedocDB}";
    };
  };

  # List services that you want to enable:
  services.locate.enable = true;
  services.btrfs.autoScrub.enable = true;

  services.jitsi-meet = {
    enable = true;
    hostName = jitsiDomain;
    interfaceConfig = {
      SHOW_JITSI_WATERMARK = false;
      SHOW_WATERMARK_FOR_GUESTS = false;
      DEFAULT_REMOTE_DISPLAY_NAME = "Fellow Anthro";
    };
    config = {
      gatherStats = false;
      disableThirdPartyRequests = true;
    };
  };

  services.hedgedoc = {
    enable = true;
    settings = {
      domain = hedgedocDomain;
      allowFreeURL = true;
      port = ports.hedgedoc;
      protocolUseSSL = true;
      urlAddPort = false;
      db = {
        dialect = "sqlite";
        storage = hedgedocDB;
      };
    };
  };

  services.gotosocial = {
    enable = true;
    settings = {
      account-domain = "leiden.digital";
      accounts-registration-open = false;
      advanced-cookies-samesite = "strict";
      advanced-sender-multiplier = 4;
      bind-address = "127.0.0.1";
      db-address = "${gotosocialDir}/gotosocial_${gotosocialDomain}.db";
      db-type = "sqlite";
      host = gotosocialDomain;
      landing-page-user = "d12n";
      log-client-ip = false;
      log-db-queries = false;
      log-level = "info";
      port = ports.gotosocial;
      protocol = "https";
      storage-backend = "local";
      storage-local-base-path = gotosocialDir;
    };
  };

  services.croodle = {
    enable = true;
    virtualHost = "poll.leiden.digital";
  };

  services.linklog = {
    enable = true;
    virtualHost = "href.leiden.digital";
  };

  services.glances-web = {
    enable = true;
    virtualHost = "dash.leiden.digital";
  };

  services.nginx = {
    enable = true;
    package = pkgs.nginxMainline;
    recommendedGzipSettings = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    virtualHosts = {
      "_".locations."/".return = "307 https://d12n.leiden.edu";
      "d12n.leiden.edu" = {
        root = "${d12nSite}";
        enableACME = true;
        forceSSL = true;
      };
      "leiden.digital" = {
        locations = {
          "/".return = "307 https://d12n.leiden.edu";
          "~ ^/@([0-9a-zA-Z]+)$".return = "308 https://${gotosocialDomain}/@$1";
          "/.well-known/host-meta".return = "308 https://${gotosocialDomain}/.well-known/host-meta$is_args$args";
          "/.well-known/nodeinfo".return = "308 https://${gotosocialDomain}/.well-known/nodeinfo$is_args$args";
          "/.well-known/webfinger".return = "308 https://${gotosocialDomain}/.well-known/webfinger$is_args$args";
        };
        enableACME = true;
        forceSSL = true;
      };
      "${gotosocialDomain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${builtins.toString ports.gotosocial}";
        };
      };
      "${hedgedocDomain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:${builtins.toString ports.hedgedoc}";
          proxyWebsockets = true;
          extraConfig = ''
            proxy_buffering off;
            proxy_buffers 64 4k;
            proxy_buffer_size 16k;
            proxy_busy_buffers_size 24k;
          '';
        };
      };
      "pad.leiden.digital" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${builtins.toString ports.etherpad}";
          extraConfig = ''
            proxy_hide_header Set-Cookie;
            proxy_hide_header X-Set-Cookie;
            proxy_ignore_headers Set-Cookie;
          '';
        };
      };
      "taguette.leiden.digital" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${builtins.toString ports.taguette}";
        };
      };
    };
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    ports = [ ports.ssh ];
    settings = {
      PasswordAuthentication = false;
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = emailAddress;
  };

  # Open ports in the firewall.
  networking.firewall = {
    enable = true;
    allowPing = true;
    allowedTCPPorts = [ ports.ssh 80 443 ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
